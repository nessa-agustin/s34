// EXPRESS SETUP
//  1. Import by using the 'require' directive to get access to the component of express package/dependency
const express = require('express');

// 2. Use the express() function and assign it to an app variable to create an express app or app server
const app = express();

const port = 3000;

// Middlewares
// These two .use are essential in express.
app.use(express.json()); //allows your app to read json format data
app.use(express.urlencoded({extended: true})) //allows app to read data from forms
// Routes

// Get request route
app.get('/',(request, response) => {
    // once the route is accessed, it will then send a string response containing hello world
    response.send('Hello World')
})

app.get('/hello',(request, response) => {
    response.send('Hello from /hello endpoint!')
})

// Register user route

// An array that will store user objects/documents when the "/register" route is accessed
// This will also serve as our mock database
let users = [];

// This route expectes to receive a POST request at the URI "/register"
// This will create a suer object in the "users" variable that mirrors a real world registration process
app.post('/register',(request, response) => {

    if(request.body.username !== '' && request.body.password !== ''){
        users.push(request.body)
        console.log(users)
        response.send(`User ${request.body.username} successfully registered.`)
    }else{
        response.send('Please input both username and password')
    }
})

// Put request route
app.put('/change-password', (request, response) => {
    let message;

    for(let i = 0; 1 < users.length; i++){
        if(request.body.username == users[i].username){
            users[i].password = request.body.password;
            message = `User ${request.body.username}'s password has been updated`
            break;
        }else{
            message = 'User does not exist'
        }
    }

    response.send(message);

})



app.get('/home',(request, response) => {
    response.send('Welcome to the home page');
})


app.get('/users',(request, response) => {
    
    response.send(users)
})

app.delete('/delete-user',(request, response) => {
    let message;

    for(let i = 0; 1 < users.length; i++){
        if(request.body.username == users[i].username){
            users.splice(i,1);
            message = `User ${request.body.username} has been deleted`
            break;
        }else{
            message = 'User does not exist'
        }
    }
    console.log(users);
    response.send(message);

})


app.listen(port, () => console.log(`Server running at port ${port}`))